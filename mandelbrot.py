# Q1 mandelbrot
# Name: Wang You
# SBID: 112871860


from PIL import Image, ImageDraw


# Step 1: set the parameters
N_max = 100       # the max number of interations
threshold = 50     # the upper bound of |z|


# Step 2: create a mandelbrot function
# input: c (complex)
# output: n (int)
def mandelbrot(c:complex):
    z = 0
    n = 1
    while n <= N_max and abs(z) <= threshold:
        z = z*z + c    # update the value of z
        n += 1        # update the number of iterarions
    return n          # record the number of iterations for each c


# Step 3: Create a canvas with white blackground
im = Image.new('RGB',(300,300),(255,255,255))   
draw = ImageDraw.Draw(im)


# Step 4: Plot c (it's real and imaginary section) on a 300 by 300 grid
for x in range(-200,  100,  1):
    for y in range(-150,  150,  1):
        c = x/100 + 1j*y/100   # convert pixel coordinate to complex number
        m = mandelbrot(c)      # compute the number of iterations
        color = 255 - int(m / N_max * 255)  # determine the color of each point (1)
        draw.point([(x+200),(y+150)], (color, color, color))   # plot the point
im.save('mandelbrot.png')        


# (1)the more iterations required by c , the darker its corresponding point will be 