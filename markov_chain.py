
# Q2 Markoc Chain
# Name: Wang You
# SBID: 112871860


import numpy as np
import math

# Steo 1: Construct a transition matrix "P"
P_raw = np.random.random((5,5))
P = P_raw / P_raw.sum(axis=1)             # normalize each row

# Step 2: Consttruct a state vector "p"
p_raw = np.random.random((5,1))
p = p_raw / p_raw.sum(axis=0)             # normalize coloumn

# Step 3: Apply the transition rule 50 times
p_50 = np.dot(pow(P.T,50),p)

# Step 4: Compute the stationary distribution
values, vectors = np.linalg.eig(P.T)   # find the eigenvalue and eigenvectors
v1 = vectors[:,0]         # extract the first colum of matrix "vectors"
Stationary = v1 / v1.sum(axis=0)   # normalize column
Stationary = Stationary.reshape(5,1)

# Step 5: Compute the component wise difference 
difference = abs(np.subtract(p_50, Stationary))  

# Step 5: Check whether the p_50 and Stationary match with each other within exp(-5)                                             

for i in range(0,5,1):
    if difference[i] > math.exp(-5):
        print('UNMATCH')
    else:
        print('MATCH')





